package me.vassilios.campfirebuff.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class OnPlayerMove implements Listener {

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		if (event.getPlayer() instanceof Player) {
			Player player = event.getPlayer();
			Location location = player.getLocation();
			int radius = 10;
			
			outerloop:
			for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
				for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
					for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
						if (location.getWorld().getBlockAt(x, y, z).getType().equals(Material.CAMPFIRE)) {
							player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 5*20, 1));
							break outerloop;
						}
					}
				}
			}
		}
	}
}
