package me.vassilios.campfirebuff;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import me.vassilios.campfirebuff.listeners.OnPlayerMove;

public class Main extends JavaPlugin {
	
	@Override
	public void onEnable() {
		registerEvents(this, new OnPlayerMove());
	}
	
	@Override
	public void onDisable() { 

	};
	
	public static void registerEvents(Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
}
